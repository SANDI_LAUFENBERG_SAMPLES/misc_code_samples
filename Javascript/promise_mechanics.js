// JavaScript Document

/*
 *----------------------------------------------------------------------------------------------------------------------
 *      _              _       ____                        _   ____                _            _   _                 
 *     | |    __ _  __| |_   _/ ___|_      __ ___  _ __ __| | |  _ \ _ __ ___   __| |_   _  ___| |_(_) ___  _ __  ___ 
 *     | |   / _` |/ _` | | | \___ \ \ /\ / /  _ \| '__/ _` | | |_) | '__/ _ \ / _` | | | |/ __| __| |/ _ \| '_ \/ __|
 *     | |__| (_| | (_| | |_| |___) \ V  V /\ (_) | | | (_| | |  __/| | | (_) | (_| | |_| | (__| |_| | (_) | | | \__ \
 *     |_____\__,_|\__,_|\__, |____/ \_/\_/ \____/|_|  \__,_| |_|   |_|  \___/ \__,_|\__,_|\___|\__|_|\___/|_| |_|___/
 *                       |___/                                                                                       
 *
/***********************************************************************************************************************
/***********************************************************************************************************************
------------------------------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------------------------------

--->>>          NOTE:  If you have any tips on how I can make this more informative re: my skills, please don't     <<<---
--->>>                  hesitate to contact me at:                                                                  <<<---
--->>>                      http://www.4jc.us/support/whmcs/contact.                                                <<<---
--->>>                          or                                                                                  <<<---
--->>>                      http://www.ladysword.net/main2/index.php/support/contact-us                             <<<---
--->>>                                                                                                              <<<---
--->>>                                                              -Sandi                                          <<<---

--------------------------------------------------------------------------------------------------------------------------



'
'   Programmer's Name:   Sandi Laufenberg-Deku, Software Developer
'                        LadySword Productions
'                        www.Ladysword.net
                         262-777-8788
'                        CONTACT FORM:  http://www.4jc.us/support/whmcs/contact.php
                                            or 
                                        http://www.ladysword.net/main2/index.php/support/contact-us
'   Date Page Started:   Nov 2, 2017
    
'   PLEASE FEEL FREE TO CONTACT ME IF YOU HAVE ANY QUESTIONS OR
'       NEED HELP WITH THIS PROJECT IN THE FUTURE.

'   APP NAME:       Incognito (HH)
'
'   This file:      promise_mechanics.js

'   Purpose:        Holds the functions related to the Javascript Promises
'
'   Resources:      There are a number of Documentation files available to assist the developer.
'                   There are notes, Word docs, Visio diagrams, and others.  Please feel free to contact 
                    Sandi if you need a copy.  I
'                   fully believe in making such materials available to future developers at any such
'                   time as they are requested, and with no hesitation.
'   Users:          

'   Revisions:
'       DATE        WHO DID THEM        WHAT WAS DONE
'       ----        ------------        -------------
'
'*--------------------------------------------------------------------------  -->
/* 

TO DO LIST:
      

OTHER NOTES:
                   
                
*/








/*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                                                                                                                                   /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                  TESTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */       
function functRunTest()
{

  alert('inside functRunTest function. AND globalCurrentHtmlPage: >>>' + 
                            globalCurrentHtmlPage + '<<<. Marker 03217.1042b.');

}


function functTestCREATEfailure()
{
  
  if (2 + 4 === 9) 
    {
        return true;
      } else {        
        return false;
      }  
}
/* -------------------------------------------
  Put this snippet inside the promise being 
  tested.  Keep this SNIPPET with the function 
  it calls for ease of use */
      /* ----------------------------------
            use this to create a fail
              for testing failure 
              / error catching
        note: change 6 to 9 so the if test bombs
        ------------------------------------- */
        /* 
        var varReturnNegative = "Process failed.";
        if (functTestCREATEfailure()) {
            successful("YIPPEE !!!  we were successful");
          } else {        
            failure(varReturnNegative);
          }    
//------------------------------------------- */

// ~~~~~~~~~~~~~~~ END TESTS ~~~~~~~~~~~~~~~~~~~~


 

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            PROMISES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */ 

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          50.2.2b.05.3
  SetCurrentHtmlPageTo_add_item_1_1_a2_htmPromise
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   CALLED FROM:  functLoadCatPgUsingPromises()      */
function SetCurrentHtmlPageTo_add_item_1_1_a2_htmPromise()
{
  return new RSVP.Promise(function(successful, failure)
  {

    /* functDisplayAlert('inside return new RSVP.Promise function. will now set the page. globalCurrentHtmlPage =  >>>' + 
                          globalCurrentHtmlPage + '<<<. Marker 03217.1042c.');
    */

    var globalCurrentHtmlPage = "add_item_1_1_a2";
      
    var varSendThisText = "";
                                                      if (globalDisplayDevAlerts === "z") 
                                                      {
                                                            varSendThisText = ('inside setCurrHtmlPageTo_add_item_1_1_a2_htm function. AND we just set the page to: >>>' + 
                                                                                globalCurrentHtmlPage + '<<<. Marker 03217.1042a.');
                                                            functDisplayAlert(varSendThisText);
                                                      }

    successful();
    }); 
}




/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        50.2.2b.05.6
  ic_ListAll_xh_item_categories_Promise
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   Prepare list of all categories and display as clickable listview on a new html pg. 
   CALLED FROM:  functLoadCatPgUsingPromises()      */  
var ic_ListAll_xh_item_categories_Promise = function () {
  return new RSVP.Promise(function(successful, failure)
  {
    var varSendThisText = "";
    var thisCategorySortID = "";

                                      if (globalDisplayDevAlerts === "z ") 
                                      {
                                        varSendThisText = ('inside ic_ListAll_xh_item_categories_Promise. Marker 8255.');
                                        functDisplayAlert(varSendThisText);
                                      }
  
    
      globalProgressMarker = "Marker 8257";
    
      // ------------------
      //   marker 8259
      // ------------------
      // this line clears out any content in the html element (i.e. #lbUsers) on the page so that we can fill it with fresh content.  
      //  alternate name: insertedHTML_displayItemCategoryListviewForSearching
      $('#lbList_All_Categories_Here').html('');

      // this next section will select all the content from the xh_item_category_lookup table and then go through it 
      //  row by row appending the UserId  FirstName  LastName to the  #lbUsers element on the page
      this_apps_db.transaction(function(transaction) {

        
                                        globalProgressMarker = "Marker a10";
    
                                        // // // alert(' just inside trx function for function ListAll_SQLite_Tables_on_This_Device().   Marker abc1010.');
                                      if (globalDisplayDevAlerts === "4") 
                                        {
                                          varSendThisText = ('Just inside trx function for function ic_ListAll_xh_item_categories(). ' +
                                                      ' Marker 021517.014.');
                                          functDisplayAlert(varSendThisText);
                                        }
                                    
        strQuery = selectAll_xh_item_category_lookup_RecordsSQLquery + 
              " ORDER BY ic_add_item_process_sort_order";
                                        globalProgressMarker = "Marker a14";
    
        transaction.executeSql(strQuery, [],
          function(transaction, result) {
        
                                      globalProgressMarker = "Marker a17";
    
            // IF WE DO HAVE RECORDS FROM OUR SQL QUERY, THEN BUILD OUR DISPLAY
            //  && MEANS (Logical AND) Returns true if both logical operands are true. Otherwise, returns false.
            if (result !== null && result.rows !== null) {
            
            // ----- CONTENTS OF LIST VIEW -----------
            // ui-corner-all: Adds rounded corners to the element. ui-shadow: Adds an item shadow around the element. ui-overlay-shadow: Adds an 
            //  overlay shadow around
            // // // var content =  '<div ui-corner-all ui-shadow ><ul id="users_listview" data-theme = "c" data-role="listview" data-theme="b" data-inset="true">';
            
            
                                      globalProgressMarker = "Marker 11017.125.";
  
            // ----- STARTING TAG FOR LIST VIEW ------
            var content =  '<div id="autodividers-linkbar-container">'+
                      '<ul data-role="listview" id="all_item_categories_listview" data-theme="c" data-autodividers="true" data-filter="true" data-filter-placeholder="Search categories..." data-inset="false">';
                      
            
                                      globalProgressMarker = "Marker a25";
            
            // ----- CONTENTS OF LIST VIEW -----------  
            for (var i = 0; i < result.rows.length; i = i + 1) 
            {
              var row = result.rows.item(i);
              thisCategorySortID = row.ic_add_item_process_sort_order; 
              // NOTE:  parameter sent should be a number type (not string). I tried sending a string but
              //      it is very troublesome. This click will end up opening this page, 
              //      add_item_1_1_a3_type.htm - but only the category num is saved to the dbase. 
              //        - Sandi 2-8-17
              content = content + '<li><a href="" onclick="functOpenItemTypeList1(' + thisCategorySortID + ')" data-ajax="false">';
                  content = content + row.ic_name;
              content = content + '</a>';
              content = content + '</li>';
              if (i === 0) {
                  // the first rows may end up outputting an empty set due to user being able to save an empty records
                  //  so make sure if you get an empty row, that you add validation to the data entry for that table to 
                  //  prevent empty records from being saved. 
              }
            }  // END OF FOR NEXT LOOP 
            
                                      globalProgressMarker = "Marker 8295";
    
            content = content + '</ul><!-- /listview id="all_item_categories_listview" -->';
            content = content + '</div> <!--  /id="autodividers-linkbar-container" -->';
            
            // --------- END OF CONTENTS OF LIST VIEW -------------
            
            

            /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        50.2.2b.07.8 
                  Display ALL Categories      
              ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
            // Use this line to put the new content into a certain place, like a span or a div. 
            $('#lbList_All_Categories_Here').html(content).enhanceWithin();
            
            } else 
            {
          
              varSendThisText = ('There are no records to display. Marker 8296');
              functDisplayAlert(varSendThisText);
                
            } //  ~~~~~~~~~~~ END OF IF TEST - TESTING FOR NULL OR EMPTY RECORDSET 
         
    /*  
    // not sure if we need thiese here or not
          },errorHandler);
      },errorHandler,nullHandler);
      */

          },errorHandler);
      },errorHandler,nullHandler);

    successful();
    });
};




// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//            50.2.2b.10.2a.1
//  StoreSelectedCategoryNumPromise()                        
//        STORE CATEGORY NUM
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// called from:  add_item_1_1_a2_DELETE_NOT_IN_USE.htm
function StoreSelectedCategoryNumPromise(categorySortNum)
{
  return new RSVP.Promise(function(successful, failure)
  {
      var varSendThisText = "";
      var varDateStamp = functGetNowDate();
      var varUpdatedByWhom = "Sandi Using Device-FIX LATER";

      var strQuery = 'UPDATE zz_CONFIG_DEVICE_SPECS SET ' +
                          '"spec_value_of_current_parameter" = ?, ' +
                          '"spec_date_updated" = ?, ' +
                          '"spec_updated_by" = ? ' +
                          'WHERE "spec_parameter_name" = ?';

      this_apps_db.executeSql(strQuery, [categorySortNum,varDateStamp,varUpdatedByWhom,"Category Sort Num"]);                 

      


      successful();
  });
}




/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//             50.2.2b.10.2a.4
//         TxExeSqlCallbackPromise()                        
//      CONTINUE DISPLAYING ITEM TYPES
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// called from:    functMakeItemTypeListUsingPromises()     */  
var TxExeSqlCallbackPromise = function () {
    return new RSVP.Promise(function(successful, failure)
  {
                                    var varSendThisText = "";
                                    globalProgressMarker = "0212117_0741p";
                                     
        var varNoDate = "NO DATE";
        varSendThisText = ('Please Select the TYPE of Item. (Necessary in order to organize your items.)');
        functDisplayAlert(varSendThisText, varNoDate);
          

      // ======================
      //  50.2.2b.10.2a.4.5
      // ======================
      // now open the next page. 
      window.open("add_item_1_1_a3_type.htm");

      successful();
  });
};




 function functSandiTestTempSQLDELETETHIS()
{

  // SELECT * FROM zz_CONFIG_DEVICE_SPECS WHERE PARAMETER_NAME = "Category Sort Num"    
  // WHERE PARAMETER_NAME = "Category Sort Num"
  var varSendThisText = "";
  globalProgressMarker = '021617a';

    if (globalDisplayDevAlerts === "8") 
    {
      varSendThisText = ('globalProgressMarker = >>>" + globalProgressMarker + "<<<. Marker 120117.977.');
      functDisplayAlert(varSendThisText);
    }

  this_apps_db.transaction(function(transaction) {
    globalProgressMarker = '021617b';
    
    //transaction.executeSql('SELECT * FROM "h_items"', [], 
    //transaction.executeSql('SELECT * FROM "xh_item_category_lookup"', [], 
    transaction.executeSql('SELECT * FROM "zz_CONFIG_DEVICE_SPECS"', [], 
      function(ignored, resultSet) {
        globalProgressMarker = '021617c';
        varSendThisText = ('UR RECORD: ' + JSON.stringify(resultSet.rows.item(0)));
        
        functDisplayAlert(varSendThisText);
      
      },errorHandler   // ~~~~~~~~~ END function for transaction.executeSql ~~~~~~~~~
    );            // ~~~~~~~~~ END transaction.executeSql ~~~~~~~~~
  }, function(error) {
      varSendThisText = ('SELECT error: ' + error.message + "| Inside functSandiTestTempSQL(). ");
      functDisplayAlert(varSendThisText);
     }
  );  // ~~~~~~~~~ END DB.TRANSACTION ~~~~~~~~~


}





                      


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//             50.2.3.3
//     StartListing_xh_Item_TypesPromise()                        
//      CONTINUE DISPLAYING ITEM TYPES
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//   formerly this was done via the function "it_ListAll_xh_item_types" (ZZ config device specs js file)
// called from:    functLoad_ItemTypeListUsingPromises()      */  
// // // function StartListing_xh_Item_TypesPromise {
var StartListing_xh_Item_TypesPromise = function () {
  return new RSVP.Promise(function(successful, failure)
  {
        var varSendThisText = "";
        var varPulledCatNum = "";
        var varSpec_parameter_name = "";
        
        document.addEventListener('deviceready', function() {

            globalProgressMarker = "Marker gz500";
        
            /* ~~~~~~~~~~~~~~~~~~~~~~~~
                 50.2.3.3.2
                Get Category Num
            ~~~~~~~~~~~~~~~~~~~~~~~~~~~
            first, Pull num  and name from database  */
                strQryGetCatNum = 'SELECT spec_parameter_name, ' +                  
                            'spec_value_of_current_parameter, ' +
                            'spec_date_updated, ' +
                            'spec_updated_by ' +                      
                          'FROM "zz_CONFIG_DEVICE_SPECS" ' +
                          'WHERE spec_parameter_name = "Category Sort Num"';
                globalProgressMarker = "Marker gz510";
        

                /*  SYNTAX:
                        executeSql(sqlStatement, arguments, callback, errorCallback)  */
                //transaction.executeSql('SELECT * FROM "zz_CONFIG_DEVICE_SPECS" WHERE spec_parameter_name = "Category Sort Num"', [], 
                this_apps_db.executeSql(strQryGetCatNum, [],
                  function(resultSet) 
                  {
                    globalProgressMarker = '8550.1';
                    
                    if (resultSet !== null && resultSet.rows !== null) {
                        alert("test4 50.2.3.3.2. ResultSet is not empty");

                        // ----- GO THRU CONTENTS OF RECORDSET -----------
                        for (var i = 0; i < resultSet.rows.length; i = i + 1) 
                        {
                          var varThisRow = resultSet.rows.item(i);
                          varSpec_parameter_name  = varThisRow.spec_parameter_name; 
                          varPulledCatNum         = varThisRow.spec_value_of_current_parameter;  

                          // ----- DISPLAY PULLED CAT NUM -----------
                          alert('parameters pulled from db: ' + varSpec_parameter_name + 
                                                            ' = >>>' + varPulledCatNum + 
                                                            '<<<.  globalProgressMarker = ' + globalProgressMarker); 

                          
                          if (i === 0) {
                              // the first rows may end up outputting an empty set due to user being able to save 
                              //  an empty records so make sure if you get an empty row, that you add validation to 
                              //  the data entry for that table to prevent empty records from being saved. 
                              //   _______ COMPLETE THIS IF TEST TASK BY CHECKING FOR EMPTY RECORD HERE - not necessary if
                              //    we are getting data. 
                          }
                        }  // END OF FOR NEXT LOOP 


  
                    } else 
                    {
                        alert("test4 50.2.3.3.2. ResultSet is empty");

                        varSendThisText = ('There are no records to display. Marker 021417.015');
                        functDisplayAlert(varSendThisText);
                         
                    } //  ~~~~~~~~~~~ END OF IF TEST - TESTING FOR NULL OR EMPTY RECORDSET 

                                              globalProgressMarker = '8550.2';
                                              if (globalDisplayDevAlerts === "11") 
                                              {
                                                //'WOO HOO! PRAISE JESUS! WE ARE INSIDE THE FUNCTION - MAKING PROGRESS TODAY! THANK YOU JESUS!!! ');
                                                varSendThisText = ('WOO HOO! PRAISE JESUS!' + 
                                                            '  globalProgressMarker = ' + globalProgressMarker);
                                                functDisplayAlert(varSendThisText);
                                              }
                       


                  // EXPERIMENTING CAN I USE THE HANDLER'S ERROR CALL NAME FROM THE PROMISE PARAMETERS HERE?
                  //  ______ TASK. A: not sure but I think it's not working. need to stage an error here to test it. 
                  // // // },errorHandler   // ~~~~~~~~~ END function for transaction.executeSql ~~~~~~~~~
                  },failure   // ~~~~~~~~~ END function for db.executeSql ~~~~~~~~~
              );            // ~~~~~~~~~ END db.executeSql ~~~~~~~~~
            
            
              alert("test5 50.2.3.3.2. ");
       
          // this line clears out any content in the html element (i.e. #lbList_Item_Types_Here) on the page so that 
          //  we can fill it with fresh content.  
          //  alternate name: insertedHTML_displayItemCategoryListviewForSearching
          $('#lbList_Item_Types_Here').html('<br><br>Error. Query failed to complete. Please report this to Support, and let them ' +
                            'know what you were doing, as well as the Marker number. Marker az600.');

          
          // this next section will select all the content from the table and then go through it row by row
          // appending to the  #lbList_Item_Types_Here element on the page
          globalProgressMarker = "Marker az1000";
        
            
          strQueryItemTypes = "SELECT * FROM xh_item_type_lookup" + 
              " WHERE it_category = ? " + 
              " ORDER BY it_sort_order;";
        


          this_apps_db.executeSql(strQueryItemTypes,[varPulledCatNum],
              function(rs_populate_html) {   // <<<---- this function is a callback, happens after the 
                                            //        successful execution of the query
                                              globalProgressMarker = "Marker 8550.3";
          
                                              if (globalDisplayDevAlerts === "11") 
                                              {
                                                varSendThisText = ('inside the callback for listing item types. ' +
                                                            ' query should have been successul.' + 
                                                            '  globalProgressMarker = ' + globalProgressMarker);
                                                functDisplayAlert(varSendThisText);
                                              }
                       

                    // IF WE DO HAVE RECORDS FROM OUR SQL QUERY, THEN BUILD OUR DISPLAY
                    //  && MEANS (Logical AND) Returns true if both logical operands are true. Otherwise, returns false.
                    if (rs_populate_html !== null && rs_populate_html.rows !== null) {
                    
                          // ----- CONTENTS OF LIST VIEW -----------
                          
                          // ----- STARTING TAG FOR LIST VIEW ------
                          var content =  '<div id="autodividers-linkbar-container">'+
                                    '<ul data-role="listview" id="item_types_listview" data-theme="c" data-autodividers="true" data-filter="true" data-filter-placeholder="Search categories..." data-inset="false">';
                                                        globalProgressMarker = "Marker 8550.4";
                    
                                                        if (globalDisplayDevAlerts === "11") 
                                                        {
                                                          varSendThisText = ('inside the the IF part of the IF TEST. ' +
                                                                      ' about to start the 4 next loop.' + 
                                                                      '  globalProgressMarker = ' + globalProgressMarker);
                                                          functDisplayAlert(varSendThisText);
                                                        }
                                

                          // ----- CONTENTS OF LIST VIEW -----------
                          for (var i = 0; i < rs_populate_html.rows.length; i = i + 1) 
                          {
                                
                                                        globalProgressMarker = "Marker 8550.5";
                    
                                                        if (globalDisplayDevAlerts === "11") 
                                                        {
                                                          varSendThisText = ('inside the 4 next loop.' + 
                                                                      '  globalProgressMarker = ' + globalProgressMarker);
                                                          functDisplayAlert(varSendThisText);
                                                        }


                                var varNowThisRow = rs_populate_html.rows.item(i);
                                thisItemTypeID = varNowThisRow.it_id; 
                                // NOTE:  parameter sent should be a number type (not string). I tried sending a string but
                                //      it is very troublesome. - Sandi 2-8-17
                                content = content + '<li><a href="" onclick="functOpenItemNameLookupList(' + thisItemTypeID + ')" data-ajax="false">';
                                    content = content + varNowThisRow.it_name;  
                                content = content + '</a>';
                                content = content + '</li>';
                                if (i === 0) {
                                    // the first rows may end up outputting an empty set due to user being able to save an empty records
                                    //  so make sure if you get an empty row, that you add validation to the data entry for that table to 
                                    //  prevent empty records from being saved. 
                                }
                          }  // END OF FOR NEXT LOOP 
                          
                                                        globalProgressMarker = "Marker 8550.4";
                    
                                                        if (globalDisplayDevAlerts === "11") 
                                                        {
                                                          varSendThisText = ('done with 4 next loop. i = >>>' + 
                                                                      i + '<<< ||  globalProgressMarker = ' + globalProgressMarker);
                                                          functDisplayAlert(varSendThisText);
                                                        }

 
                          content = content + '</ul><!-- /listview id="item_types_listview" -->';
                          content = content + '</div> <!--  /id="autodividers-linkbar-container" -->';
                          
                          // --------- END OF CONTENTS OF LIST VIEW -------------
                          
                          // Use this line to put the new content into a certain place, like a span or a div. 
                          $('#lbList_Item_Types_Here').html(content).enhanceWithin();
                          
                    } else 
                    {
                  
                          varSendThisText = ('There are no records to display. Marker 021417.015');
                          functDisplayAlert(varSendThisText);
                        
                    } //  ~~~~~~~~~~~ END OF IF TEST - TESTING FOR NULL OR EMPTY RECORDSET 
                 
          
          },failure);  // END transaction.executeSql 
      














            
      });
      successful();
  });
};







/* ~~~~~~~~~~~~~ END PROMISES ~~~~~~~~~~~~~~ */       





/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        MAIN FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */       

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            50.2.2b.05 
    functLoadCatPgUsingPromises()       
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CALLED FROM:   document.addEventListener('deviceready')    */
function functLoadCatPgUsingPromises()
{
    //alert("globalDisplayDevAlerts = >>>" + globalDisplayDevAlerts + "<<<");
    SetCurrentHtmlPageTo_add_item_1_1_a2_htmPromise()
                            // .then(InitDatabase_Promise)    // ~~~ WARNING !!!! ~~~~  DO NOT NOT NOT try to
        .then(ic_ListAll_xh_item_categories_Promise)            // to initialize dbase from the promises. I wasted
        .then(function(){                                       // several hours trying to do so. -sandi 3-25-17
              // alert("3rd .then inside SetCurrentHtmlPageTo_add_item_1_1_a2_htmPromise (functLoadCatPgUsingPromises)");
        },null)                          
        .catch(function(err)
        {
              console.error('Hmmmm - ERROR: ', err);                      // <---- This catches almost all errors no
                                                                          //      matter where they occur!
                varOutputString = varOutputString + 'Woops... ERROR: ';  
                varOutputString = varOutputString + err + ".  Marker 8240";    // <---- The "err" will be output last. This "err" 
                                                                            //  will take a real err msg if one is sent. If not, 
                alert('Oh no... ERROR: ', err);                                                //  it will use the value set by 
                                                                            //  the failure function in the promis                                                                            //  function 
                                                                            //  -- since that's what we return  from the promise 
                                                                            //  as parameter for the failure function.
                alert(varOutputString);                                      
        });

}



/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              50.2.3.2   
    functMakeItemTypeListUsingPromises()
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This function starts the promise chain for the Item Type list page.    functStoreSelectedCategoryNum(categorySortNum)
CALLED FROM:   add_item_1_1_a3_js.js    */
function functLoad_ItemTypeListUsingPromises()
{
    StartListing_xh_Item_TypesPromise()
        
        .then(function(){
          // dev tool:
          // // // alert("wow - here we are. Marker 8410.");
        },null)                          
        
        .catch(function(err)
        {
              console.error('ERROR: ', err);                      // <---- This catches almost all errors no
                                                                  //      matter where they occur!
                varOutputString = varOutputString + 'ERROR: ';  
                varOutputString = varOutputString + err + "Marker 8490";    // <---- The "err" will be output last. This "err" 
                                                              //  will take a real err msg if one is sent. If not, 
               alert('ERROR: ', err);                                                 //  it will use the value set by 
                                                                            //  the failure function in the promise
                                                                            //  function 
                                                                            //  -- since that's what we return  from the promise 
                                                                            //  as parameter for the failure function.
               alert(varOutputString);                                      
        });

}





/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              50.2.2b.10.2a 
    functMakeItemTypeListUsingPromises()
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This function starts a different promise chain (after the first one) for the Item Type list page.    
functStoreSelectedCategoryNum(categorySortNum)
CALLED FROM:   add_item_1_1_a3_js.js    */
function functMakeItemTypeListUsingPromises(categorySortNum)
{
  StoreSelectedCategoryNumPromise(categorySortNum)
                    // .then(InitDatabase_Promise)    // ~~~ WARNING !!!! ~~~~  DO NOT NOT NOT try to
                                      //     open the database inside the promise chain, as I 
                                      //    could not get it to work. 
    .then(TxExeSqlCallbackPromise)                  
    .then(function(){                               // several hours trying to do so. -sandi 3-25-17
          // alert("item type | 3rd .then. A3 should have OPENED, RIGHT?");
      },null)                          
    .catch(function(err)
      {
          console.error('ERROR: ', err);                      // <---- This catches almost all errors no
                                                              //      matter where they occur!
            varOutputString = varOutputString + 'What???... ERROR: ';  
            varOutputString = varOutputString + err + "Marker 8390";    // <---- The "err" will be output last. This "err" 
                                                          //  will take a real err msg if one is sent. If not, 
           alert('ERROR: ', err);                                                 //  it will use the value set by 
                                                                        //  the failure function in the promise
                                                                        //  function 
                                                                        //  -- since that's what we return  from the promise 
                                                                        //  as parameter for the failure function.
            alert(varOutputString);                                      
      });
}



/* ~~~~~~~~~ END MAIN FUNCTIONS ~~~~~~~~~~ */       

